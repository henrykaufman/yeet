var express = require('express');
var mongoose = require('mongoose');
var app = express.Router();
var passport = require('passport');
var Strategy = require('passport-local').Strategy;
var bcrypt = require('bcrypt-nodejs');
var path = require('path');
var fs = require('fs');

var conn = mongoose.createConnection("mongodb://localhost/yeet");

var RoomSchema = new mongoose.Schema({
    id: String,
    messages: [{
        user: String,
        message: String,
        date: []
    }]
});

var Room = conn.model('Room', RoomSchema);

var UserSchema = new mongoose.Schema({
    username: String,
    password: String,
    yeets: [{
        name: String
    }]
});

UserSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

UserSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

var User = conn.model('User', UserSchema);



app.get('/', function(req, res) {
    res.render('index', {
        title: 'YEET'
    });
});

app.get('/ya', function(req, res) {
    if (process.env.NODE_ENV == "production") {
        fs.readFile('public/devya.js', 'utf8', function(err, data) {
            if (err) return console.log(err);
            data = data.replace('127.0.0.1:3000', 'yeet.im');
            fs.writeFile('public/ya.js', data, 'utf8', function (err) {
                if (err) return console.log(err);
                res.sendFile(path.join(__dirname + '../../public/ya.js'));
            });
        });
    }
    else {
        res.sendFile(path.join(__dirname + '../../public/devya.js'));
    }
});

app.get('/yeet/:id', function(req, res) {
    Room.findOne({
        'id': req.params.id.toLowerCase()
    }, function(err, room) {
        if (room) {
            console.log('Room is' + room);
            res.render('home', {
                room: room,
                title: 'Room - ' + room.id
            });
        } else {
            res.redirect('/');
        }
    });
});

app.get('/show', function(req, res) {
    console.log();
    res.sendFile(path.join(__dirname + '/show.html'));
});

app.get('/data/:id', function(req, res) {
    Room.findOne({
        'id': req.params.id
    }, function(err, room) {
        res.send(room);
    });
});

app.get('/u/:id', function(req, res) {
    User.findOne({
        'username': req.params.id
    }, function(err, user) {
        if (user) {
            console.log(user.yeets);
            res.render('profile', {
                user: user.username,
                yeets: user.yeets,
                title: user.username
            });
        } else {
            res.redirect('/');
        }
    })
});

passport.serializeUser(function(user, cb) {
    cb(null, user.id);
});

passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
        done(err, user);
    });
});


app.use(require('express-session')({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());

passport.use('local-login', new Strategy(
    function(username, password, done) {
        User.findOne({
            username: username
        }, function(err, user) {
            if (err) {
                return done(err);
            }
            if (!user) {
                return done(null, false);
            }
            if (!user.validPassword(password)) {
                return done(null, false);
            }
            return done(null, user);
        });
    }
));

passport.use('local-signup', new Strategy(
    function(username, password, done) {
        User.findOne({
            username: username
        }, function(err, user) {
            if (err) {
                return done(err);
            }
            if (user) {
                return done(null, false);
            } else {
                var newUser = new User();

                // set the user's local credentials
                newUser.username = username;
                newUser.password = newUser.generateHash(password);

                // save the user
                newUser.save(function(err) {
                    if (err)
                        throw err;
                    return done(null, newUser);
                });
            }
        });
    }
));

app.post('/login', passport.authenticate('local-login', {
        failureRedirect: '/'
    }),
    function(req, res) {
        console.log('success');
        res.send({
            status: 'success',
            user: req.user
        })
    });

app.post('/signup', passport.authenticate('local-signup', {
        failureRedirect: '/'
    }),
    function(req, res) {
        console.log('success');
        passport.authenticate('local-login')(req, res, function() {
            console.log(req.user);
            res.send({
                status: 'success',
                user: req.user
            });
        });
    });

function sessionCleanup() {
    sessionStore.all(function(err, sessions) {
        for (var i = 0; i < sessions.length; i++) {
            sessionStore.get(sessions[i], function() {});
        }
    });
}

setInterval(function() {
    sessionCleanup();
}, 86400000);

module.exports = app;
