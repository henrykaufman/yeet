var express = require('express');
var mongoose = require('mongoose');
var app = express.Router();

var conn = mongoose.createConnection("mongodb://localhost/yeet");

var RoomSchema = new mongoose.Schema({
    id: String,
    messages: [{
        user: String,
        message: String,
        date: []
    }]
});

var Room = conn.model('Room', RoomSchema);

var UserSchema = new mongoose.Schema({
    username: String,
    password: String,
    yeets: [{
        name: String
    }]
});

var User = conn.model('User', UserSchema);

app.post('/newRoom', function(req, res) {
    console.log(req.body);
    Room.findOne({
        'id': req.body.id
    }, function(err, room) {
        if (!room) {
            var newRoom = new Room({
                id: req.body.id,
            });
            newRoom.save(function(err, newRoom) {
                if (err) return console.error(err);
                console.log(newRoom);

            });
            User.findOneAndUpdate({
                'username': req.body.user
            }, {
                $push: {
                    'yeets': {
                        name: req.body.id
                    }
                }
            }, {
                safe: true,
                upsert: true
            }, function(err, user) {
                console.log('adding');
                if (err) return console.log(err);
                if (user) {
                    console.log(user);
                    res.send(req.body.id);
                } else {
                    res.send('Issue');
                }
            });
        } else {
            res.send(room.id);
        }
    });
});

app.post('/sendChat', function(req, res) {
    var id = req.body.id;
    var user = req.body.user;
    var message = req.body.message;
    var date = req.body.date;
    console.log(req.body);
    Room.findOneAndUpdate({
        'id': id
    }, {
        $push: {
            'messages': {
                user: user,
                message: message,
                date: date
            }
        }
    }, {
        safe: true,
        upsert: true
    }, function(err, convo) {
        console.log('adding');
        if (err) return console.log(err);
        if (convo) {
            console.log(convo);
            res.send('Good');
        } else {
            res.send('Issue');
        }
    });
});

module.exports = app
