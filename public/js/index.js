Array.prototype.unique = function () {
    var r = new Array();
    o:for(var i = 0, n = this.length; i < n; i++)
    {
    	for(var x = 0, y = r.length; x < y; x++)
    	{
    		if(r[x]==this[i])
    		{
    			continue o;
    		}
    	}
    	r[r.length] = this[i];
    }
    return r;
}

var app = angular.module('app', ['luegg.directives']);

app.controller('IndexCtrl', ['$scope', '$http', function($scope, $http) {
    $scope.user = "";
    if (localStorage.getItem('yeetUsername')) {
        console.log(localStorage.getItem('yeetUsername'))
        $scope.user = localStorage.getItem('yeetUsername');
    }
    $scope.signup = function(id) {
        if (id) {
            if (!(/\s/.test(id))) {
                var data = {
                    id: id.toLowerCase(),
                    user: $scope.user
                }
                console.log(data);
                $http.post('/api/newRoom', data)
                    .success(function(x) {
                        console.log(x);
                        console.log(x);
                        window.location = "/yeet/" + x;
                    });
            } else {
                $scope.message = "You have a space."
            }
        }
    }
    $scope.signupUser = function(user) {
        if (user) {
            var data = {
                username: user.username,
                password: user.password
            }
            $http.post('/signup', data)
                .success(function(x) {
                    if (x.status == "success") {
                        $scope.user = x.user.username;
                        localStorage.setItem('yeetUsername', $scope.user);
                    }
                });
        }
    }
    $scope.loginUser = function(user) {
        if (user) {
            var data = {
                username: user.username,
                password: user.password
            }
            $http.post('/login', data)
                .success(function(x) {
                    if (x.status == "success") {
                        $scope.user = x.user.username;
                        localStorage.setItem('yeetUsername', $scope.user);
                    }
                });
        }
    }
}]);

app.controller('HomeCtrl', ['$scope', '$http', function($scope, $http) {

    $scope.user = "";
    if (localStorage.getItem('yeetUsername')) {
        console.log(localStorage.getItem('yeetUsername'))
        $scope.user = localStorage.getItem('yeetUsername');
    }

    $scope.room = {};

    var socket = io();

    $scope.$watch('room', function() {
        socket.emit('subscribe', $scope.room.id);
        timeago();
        funcCurrentUsers();
    });

    $scope.sendChat = function(sent) {
        if (sent.message) {
            socket.emit('send message', {
                id: $scope.room.id,
                message: sent.message,
                user: $scope.user,
                date: [new Date()]
            });
            var data = {
                id: $scope.room.id,
                user: $scope.user,
                message: sent.message,
                date: [new Date()]
            }
            $http.post('/api/sendChat', data)
                .success(function(data) {
                    console.log(data);
                });
            sent.message = '';
        }
    }

    socket.on('conversation private post', function(data) {
        console.log(data);
        $scope.$apply(function() {
            $scope.room.messages.push(data);
            timeago();
            funcCurrentUsers();
        });
        if ($scope.user != data.user) {
            var audio = new Audio('../audio/ding.wav');
            audio.play();
        }
    });

    $scope.signupUser = function(user) {
        if (user) {
            var data = {
                username: user.username,
                password: user.password
            }
            $http.post('/signup', data)
                .success(function(x) {
                    if (x.status == "success") {
                        $scope.user = x.user.username;
                        localStorage.setItem('yeetUsername', $scope.user);
                    }
                });
        }
    }
    $scope.loginUser = function(user) {
        if (user) {
            var data = {
                username: user.username,
                password: user.password
            }
            $http.post('/login', data)
                .success(function(x) {
                    if (x.status == "success") {
                        $scope.user = x.user.username;
                        localStorage.setItem('yeetUsername', $scope.user);
                    }
                });
        }
    }

    $scope.logout = function() {
        $scope.user = "";
        localStorage.removeItem('yeetUsername');
    }

    setInterval(function() {
        timeago();
        funcCurrentUsers();
    }, 5000);

    function timeago() {
        for (var i = 0; i < $scope.room.messages.length; i++) {
            console.log($scope.room.messages[i].date);
            $scope.room.messages[i].timeago = moment($scope.room.messages[i].date[0]).fromNow();
        }
    }
    function funcCurrentUsers() {
        var users = [];
        for (var i = 0; i < $scope.room.messages.length; i++) {
            var fifteen = 60 * 1000 * 15;
            var date = new Date - new Date($scope.room.messages[i].date);
            if (date < fifteen) {
                users.push($scope.room.messages[i].user);
            }
        }
        $scope.users = users.unique();
    }
}]);
