Array.prototype.unique = function () {
    var r = new Array();
    o:for(var i = 0, n = this.length; i < n; i++)
    {
    	for(var x = 0, y = r.length; x < y; x++)
    	{
    		if(r[x]==this[i])
    		{
    			continue o;
    		}
    	}
    	r[r.length] = this[i];
    }
    return r;
}

$(document).ready(function() {
    var username = "";

    var script = "<script src='http://127.0.0.1:3000/js/socket.io.js'></script>"
    script += "<script src='http://127.0.0.1:3000/js/moment.js'></script>";
    $('body').append(script);

    $.get('http://127.0.1:3000/show', function(data) {
        $('body').append(data);
        $('.ya-yeet-ya-currentUsers').hide();
        $('#ya-yeet-ya-close').hide();
        $('.ya-yeet-ya-loggedIn').hide();
        $('.ya-yeet-ya-miniPopup').hide();
        if (localStorage.getItem('yeetUsername')) {
            $('.ya-yeet-ya-login').hide();
            $('.ya-yeet-ya-loggedIn').show();
            username = localStorage.getItem('yeetUsername');
        }
    });


    var messages = [];

    setTimeout(function() {
        $('#ya-yeet-ya-roomID').html(YeetRoomID);

        socket = io.connect('http://127.0.1:3000');
        socket.emit('subscribe', YeetRoomID);

        socket.on('conversation private post', function(message) {
            $('.ya-yeet-ya-panel-body').append('<div class="ya-yeet-ya-box"><b>' + message.user + '</b><small data-date="' + message.date + '">' + message.date + '</small><p>' + message.message + '</p></div>');
            if (username != message.user ) {
                var audio = new Audio('../audio/ding.wav');
                audio.play();
            }
            $(".ya-yeet-ya-panel-body").animate({
                scrollTop: $('.ya-yeet-ya-panel-body').height()
            }, "slow");
            messages.push(message);
            timeago();
            funcCurrentUsers();
        });

        $.get('http://127.0.1:3000/data/' + YeetRoomID, function(data) {
            data.messages.map(function(message) {
                messages.push(message);
                console.log(message);
                $(".ya-yeet-ya-panel-body").animate({
                    scrollTop: $('.ya-yeet-ya-panel-body').height()
                }, "slow");
                $('.ya-yeet-ya-panel-body').append('<div class="ya-yeet-ya-box"><b>' + message.user + '</b><small data-date="' + message.date + '">' + message.date + '</small><p>' + message.message + '</p></div>');
            });
            timeago();
            funcCurrentUsers();
        });

        $('#ya-yeet-ya-submitMessage').click(function() {
            submit();
        });

        $('#ya-yeet-ya-message').keypress(function(e) {
            if (e.which == 13) {
                submit();
            }
        });

        $('#ya-yeet-ya-usersButton').click(function() {
            $('.ya-yeet-ya-currentUsers').show();
            $('#ya-yeet-ya-close').show();
            if(username) {
                $('#ya-yeet-ya-logoutButton').show();
            }
            else {
                $('#ya-yeet-ya-logoutButton').hide();
            }
        });

        $('#ya-yeet-ya-close').click(function() {
            $('.ya-yeet-ya-currentUsers').hide();
            $('#ya-yeet-ya-close').hide();
        });

        $('#ya-yeet-ya-signupButton').click(function() {
            var data = {
                username: $('#ya-yeet-ya-usernameField').val(),
                password: $('#ya-yeet-ya-passwordField').val()
            }
            $.post('http://127.0.1:3000/signup', data, function(response) {
                if (response.status == "success") {
                    $('.ya-yeet-ya-login').hide();
                    $('.ya-yeet-ya-loggedIn').show();
                    username = response.user.username;
                    localStorage.setItem('yeetUsername', username);
                }
            })
        });

        $('#ya-yeet-ya-loginButton').click(function() {
            var data = {
                username: $('#ya-yeet-ya-usernameField').val(),
                password: $('#ya-yeet-ya-passwordField').val()
            }
            $.post('http://127.0.1:3000/login', data, function(response) {
                if (response.status == "success") {
                    console.log(response);
                    $('.ya-yeet-ya-login').hide();
                    $('.ya-yeet-ya-loggedIn').show();
                    username = response.user.username;
                    localStorage.setItem('yeetUsername', username);
                }
            })
        });

        $('#ya-yeet-ya-logoutButton').click(function() {
            $('.ya-yeet-ya-currentUsers').hide();
            $('#ya-yeet-ya-close').hide();
            $('.ya-yeet-ya-footer').hide();
            localStorage.removeItem('yeetUsername');
            username = "";
        });

        $('#ya-yeet-ya-proceed').click(function() {
            $('.ya-yeet-ya-login').hide();
            $('.ya-yeet-ya-loggedIn').show();
            $('.ya-yeet-ya-footer').hide();
        });


        $('.ya-yeet-ya-miniPopup').click(function() {
            $('.ya-yeet-ya-home').show();
            $('.ya-yeet-ya-miniPopup').hide();
        });

        $('#ya-yeet-ya-hideBox').click(function() {
            $('.ya-yeet-ya-miniPopup').show();
            $('.ya-yeet-ya-home').hide();
        });
        $('#ya-yeet-ya-hidbox').click(function() {
            $('.ya-yeet-ya-miniPopup').show();
            $('.ya-yeet-ya-home').hide();
        });

    }, 500);


    function submit() {
        if ($('#ya-yeet-ya-message').val()) {
            var data = {
                id: YeetRoomID,
                message: $('#ya-yeet-ya-message').val(),
                user: username,
                date: new Date()
            }
            socket.emit('send message', {
                id: YeetRoomID,
                message: $('#ya-yeet-ya-message').val(),
                user: username,
                date: new Date()
            });
            $.post('http://127.0.1:3000/api/sendChat', data, function(response) {
                console.log(response);
                $('#ya-yeet-ya-message').val("");
            });
        }
    }

    function timeago() {
        $('.ya-yeet-ya-box small').each(function() {
            var val = moment($(this).attr('data-date')).fromNow();
            $(this).html(val);
        });
    }

    function funcCurrentUsers() {
        var users = [];
        for (var i = 0; i < messages.length; i++) {
            var fifteen = 60 * 1000 * 15;
            var date = new Date - new Date(messages[i].date);
            if (date < fifteen) {
                users.push(messages[i].user);
            }
        }
        users = users.unique();
        var html = "";
        for (var i = 0; i < users.length; i++) {
            html += "<i class='fa fa-circle ya-yeet-ya-available'>&nbsp;&nbsp;</i><p class='ya-yeet-ya-users'>" + users[i] + "</p>"
            $('.ya-yeet-ya-users').remove();
            $('.ya-yeet-ya-available').remove();
        }
        $('.ya-yeet-ya-users').remove();
        $('.ya-yeet-ya-availability').append(html);
    }
    setInterval(function() {
        timeago();
        funcCurrentUsers();
    }, 5000);

});
